# test-suite2 

## 1) Implementation design for login and loyalty page

## 2) Fake login

- Emulating authentication by storing and change variable "isAuthenticated" in data.service file.
- Displaying a message for successful or failed login is implemented in login component and html files.

## 3) Loading indicators

Loading async indicators for login and loyalty page.

## 4) Logged in status

Logged in status is not persist after reload.

## 5) Adding routing to navigation buttons

checked!

## 6) Guard the /loyalty route with angular

checked!

## 7) Using API to fetch data

I have added some extra code to fetch data function and
checked!

## 8) Buttons in Loyalty

The buttons when pressed show the information and have a popover that displays the award points
