import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { DataService } from '../data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  isLoading: boolean = false;
  hasError: boolean = false;
  errorMessage: string = '';
  constructor(private dataService: DataService, private router: Router) { }

  onSubmit(form: NgForm) {
    if(!form.valid){
      return;
    }
    this.isLoading = true;
    this.hasError = false;
    const userName = form.value.username;
    const password = form.value.password;
    this.dataService.fakeLogin(userName, password).subscribe(()=>{
      if(this.dataService.hasError){
        this.isLoading = false;
        this.hasError = true;
        this.errorMessage = 'Invalid username or password.';
        form.controls['password'].reset();
      }
    });
  }

}
