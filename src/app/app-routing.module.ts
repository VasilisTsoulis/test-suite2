import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoyaltyComponent } from './loyalty/loyalty.component';
import { LoginComponent } from './login/login.component';
import { DataGuard } from './data.guard';
import { LoginResolverService } from './login-resolver.service';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'login'
  },
  {
    path: 'login',
    component: LoginComponent,
    resolve: [LoginResolverService]
  },
  {
    path: 'loyalty',
    component: LoyaltyComponent,
    canActivate: [DataGuard]
  },
  {
    path: '**',
    redirectTo: 'login'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
