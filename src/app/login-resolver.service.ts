import { Injectable } from '@angular/core';

import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { DataService } from './data.service';
import { Login } from './login/login.model';


@Injectable({ providedIn: 'root' })
export class LoginResolverService implements Resolve<Login> {

  constructor(private dataService: DataService){}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.dataService.fakeLogin;
  }
}
