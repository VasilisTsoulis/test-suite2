import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { delay, map, tap } from 'rxjs/operators';

@Injectable()
export class DataService {
  isAuthenticated: boolean = false;
  hasError: boolean = false;

  constructor(private httpClient: HttpClient, private router: Router) {}

  public fetchData(): Observable<any> {
    return this.httpClient
      .get<any>(
        `https://raw.githubusercontent.com/ktsangop/test-suite-data/main/data.json`
      )
      .pipe(
        delay(3000),
        map(responseData => {
          // small filter to prevent the wrong structure of data
          const postsArray: Object = {...responseData};
          return postsArray;
        })
      );
  }

  public fakeLogin(userName: string, password: string) {
    const authPassed = userName === 'Vasileios' && password === 'Tsoulis';
    return of(1).pipe(
      delay(3000),
      map(() => {
        this.isAuthenticated = authPassed;

        return authPassed ? { login: true } : { login: false };
      }),
      tap(() => {
        if(authPassed){
          this.router.navigate(['/loyalty']);
        }else{
          this.hasError = true;
        }
      })
    );
  }
}
