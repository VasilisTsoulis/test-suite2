import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DataService } from './data.service';

@Injectable({providedIn: 'root'})

export class DataGuard implements CanActivate {

  constructor(private dataService: DataService, private router: Router){}

  canActivate(
    route: ActivatedRouteSnapshot,
    router: RouterStateSnapshot
    ): boolean | Promise<boolean> | Observable<boolean> {
      if(!this.dataService.isAuthenticated){
        this.router.navigate(['/login']);
      }
      return !!this.dataService.isAuthenticated;
  }
}
