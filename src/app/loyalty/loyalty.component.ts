import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-loyalty',
  templateUrl: './loyalty.component.html',
  styleUrls: ['./loyalty.component.css']
})
export class LoyaltyComponent implements OnInit {
  data: any = {};
  objectPointer: number = 0;
  isLoading: boolean = true;
  hasError: boolean = false;
  dotDistance: number = 0;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.isLoading = true;
    this.dataService.fetchData().subscribe(
      posts => {
        this.data = posts;
        if(posts.awards.length){
          // protect devision from 0
          this.dotDistance = Math.floor(100 / posts.awards.length);
        }
        this.isLoading = false;
      },
      error => {
        console.log(error);
        this.hasError = true;
      }
    );
  }

  changeLabel(i:number) {
    this.objectPointer = i;
  }

}
